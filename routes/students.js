const express = require('express')
const router = express.Router()

const students = [
    {
        id: '001',
        name: 'Tanapat Sakultalakul',
        studentid: '61160251',
    }
]

router.get('/', function (req, res) {
    res.json(students)
})

module.exports = router