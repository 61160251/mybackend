const express = require('express')
const router = express.Router()

const teachers = [
  {
    id:'001',
    name:'Arabic Cobra ',
    role:'adviser'
  },
  {
    id:'002',
    name:'Brabic Dragon',
    role:'subject'
  },
  {
    id:'003',
    name:'Dreck Koona',
    role:'dean'
  }
    

]


router.get('/', function (req, res) {
  res.json(teachers)
})

module.exports = router
