const express = require('express')
const res = require('express/lib/response')
const router = express.Router()

const documents = [
    {
        id: 1,
        user_id: '005',
        filename: '61160251_RE01',
        date: '03/12/2564',
        status:'examine',
        type:'RE01',
    },
    {
        id: 2,
        user_id: '006',
        filename: '61160251_RE02',
        date: '05/12/2564',
        status:'examine',
        type:'RE02',
    }
]
let lastId = 2 

const getDocuments = function (req,res,next){
    res.json(documents)
}

const getDocument = function(req,res,next){
    const index = documents.findIndex(function(item){
        return item.id == req.params.id 
    })
    if (index>= 0){
       res.json(documents[index]) 
    }else {
        res.status(404).json({
            code: 404,
            msg: 'No product id '+ req.params.id
        })
    }
    
}

// const getDocumentbyUser_id = function(req,res,next){
//     const index = documents.findIndex(function(item){
//         return item.user_id == req.params.user_id
//     })
//     res.json(documents[index])
// }

const addDocuments = function (req,res,next){
    console.log(req.body)
    const newDocuments = {
        id: lastId,
        user_id: req.body.user_id,
        filename: req.body.filename,
        date: req.body.date,
        status: req.body.status,
        type: req.body.type,
    }
    documents.push(newDocuments)
    lastId++
    res.status(201).json(newDocuments)
}



router.get('/', getDocuments)
router.get('/:id', getDocument)
// router.get('/:user_id', getDocumentbyUser_id)
router.post('/', addDocuments)


module.exports = router