const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const indexRouter = require('./routes/index')
const teachersRouter = require('./routes/teachers')
const studentsRouter = require('./routes/students')
const officersRouter = require('./routes/officers')
const documentsRouter = require('./routes/documents')
const usersRouter = require('./routes/users')

const app = express()
app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/teachers', teachersRouter)
app.use('/students', studentsRouter)
app.use('/officers', officersRouter)
app.use('/documents', documentsRouter)
app.use('/users', usersRouter)

module.exports = app
